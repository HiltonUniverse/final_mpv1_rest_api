from werkzeug.security import safe_str_cmp
from models.user_model import UserModel

#Authemticate the user
def authenticate(username, password):
    user = UserModel.find_by_username(username) #search in db
    if user and safe_str_cmp(user.password,password):
        return user

#we can directly retrive the user using id.
def identity(payload):
    user_id = payload['identity']
    return UserModel.find_by_id(user_id)
