import os

from flask import Flask, render_template
from flask_restful import Api
from flask_jwt import JWT

from security import authenticate, identity
from resources.user import UserRegister
from resources.customer import Customer,CustomerList,AddCustomer
from resources.sensorType import SensorType,AddSensorType,SensorTypeList
from resources.sensorNode import SensorNode,AddSensorNode,SensorNodeList
from resources.sensor import Sensor,AddSensor,SensorList
from resources.sensorData import SensorData,AddSensorData,SensorDataList

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL', 'sqlite:///data.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.secret_key = 'MPV-GATEWAY'

api = Api(app)

jwt = JWT(app, authenticate, identity)  # /auth

@app.route('/')
def static_page():
    return render_template('welcome.html')

api.add_resource(UserRegister, '/register') #calls the post method of the class UserRegister

#endpoint for customer resource
api.add_resource(CustomerList, '/customers')
api.add_resource(Customer, '/customer/<int:Id>')
api.add_resource(AddCustomer,'/addCustomer')

#endpoint for sensorType resource
api.add_resource(SensorTypeList, '/sensorTypes')
api.add_resource(SensorType, '/sensorType/<int:Id>')
api.add_resource(AddSensorType, '/addSensorType')

#endpoint for SensorNode resource
api.add_resource(SensorNodeList, '/sensorNodes')
api.add_resource(SensorNode, '/sensorNode/<int:Id>')
api.add_resource(AddSensorNode, '/addSensorNode')

#endpoint for Sensor resource
api.add_resource(SensorList, '/sensors')
api.add_resource(Sensor, '/sensor/<int:Id>')
api.add_resource(AddSensor, '/addSensor')

#endpoint for SensorData resource
api.add_resource(SensorDataList, '/sensorDatas')
api.add_resource(SensorData, '/sensorData/<int:Id>')
api.add_resource(AddSensorData, '/addSensorData')

if __name__ == '__main__':
    from db import db #Import sql_alchemy object
    db.init_app(app)#pass in our flask app.
    app.run(port = 5000, debug = True)
