from db import db

class SensorTypeModel(db.Model):
    __tablename__ = 'SensorType'

    Id = db.Column(db.Integer,primary_key=True)
    Type = db.Column(db.String(100),nullable = False)
    Name = db.Column(db.String(255),unique = True, nullable = False)
    Unit = db.Column(db.String(100),nullable = False)
    Variance = db.Column(db.Float(precision = 3))

    #back reference
    sensor = db.relationship('SensorModel', lazy = 'dynamic')

    def __init__(self,Type,Name,Unit,Variance):
        self.Type = Type
        self.Name = Name
        self.Unit = Unit
        self.Variance = Variance

    def json(self):
        return {'Id': self.Id,
                'Type': self.Type,
                'Name': self.Name,
                'Unit': self.Unit,
                'Variance': self.Variance
                }

    @classmethod
    def find_by_Id(cls,Id):
        return cls.query.filter_by(Id = Id).first()

    @classmethod
    def find_by_Name(cls,Name):
        return cls.query.filter_by(Name = Name).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
