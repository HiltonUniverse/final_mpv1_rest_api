from db import db

class CustomerModel(db.Model):
    __tablename__ = 'Customer'

    Id = db.Column(db.Integer,primary_key=True)
    FirstName = db.Column(db.String(100),nullable = False)
    LastName = db.Column(db.String(100),nullable = False)
    Email = db.Column(db.String(255),nullable = False)
    Country = db.Column(db.String(100),nullable = False)
    City = db.Column(db.String(100),nullable = False)
    Street = db.Column(db.String(100),nullable = False)
    StreetNumber = db.Column(db.String(45),nullable = False)

    #back reference
    sensorNodes = db.relationship('SensorNodeModel',lazy = 'dynamic') #defines many-to-one Relationship

    def __init__(self,FirstName,LastName,Email,Country,City,Street,StreetNumber):
        self.FirstName = FirstName
        self.LastName = LastName
        self.Email = Email
        self.Country = Country
        self.City = City
        self.Street = Street
        self.StreetNumber = StreetNumber

    def json(self):
        #return {'Customer': [sNode.json() for sNodes in self.sensorNodes.all()]}
        return {'Id': self.Id,
                'FirstName':self.FirstName,
                'LastName': self.LastName,
                'Email' : self.Email,
                'Country': self.Country,
                'City' : self.City,
                'Street' : self.Street,
                'StreetNumber' : self.StreetNumber
                }

    @classmethod
    def find_by_Id(cls,Id):
        return cls.query.filter_by(Id = Id).first()

    @classmethod
    def find_by_Email(cls,Email):
        return cls.query.filter_by(Email = Email).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
