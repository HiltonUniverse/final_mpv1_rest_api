from db import db

#User to register and retrive ADMINS
class UserModel(db.Model):
    __tablename__ = 'Users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(80))

    def __init__(self, username, password):
        self.username = username #we don't need id, as it's auto incrementing in our case
        self.password = password

    #Save Self/userModel object to Db
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod #doing this will make this method be callable like a static method. Class.method()
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id = _id).first()
