from db import db


class SensorModel(db.Model):
    __tablename__ = 'Sensor'

    Id = db.Column(db.Integer, primary_key = True)

    SensorTypeId = db.Column(db.Integer,db.ForeignKey('SensorType.Id'),nullable = False)
    sensorType = db.relationship('SensorTypeModel')

    SensorNodeId = db.Column(db.Integer,db.ForeignKey('SensorNode.Id'), nullable = False)
    sensorNode = db.relationship('SensorNodeModel')

    #backReference
    sensorData = db.relationship('SensorDataModel',lazy = True)

    def __init__(self,SensorTypeId,SensorNodeId):
        self.SensorTypeId = SensorTypeId
        self.SensorNodeId = SensorNodeId

    def json(self):
        return {'Id':self.Id,
                'SensorTypeId':self.SensorTypeId,
                'SensorNodeId':self.SensorNodeId
                }

    @classmethod
    def find_by_Id(cls,Id):
        return cls.query.filter_by(Id = Id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
