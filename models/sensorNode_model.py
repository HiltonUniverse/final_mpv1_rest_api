from db import db

class SensorNodeModel(db.Model):
    __tablename__ = 'SensorNode'

    Id = db.Column(db.Integer,primary_key=True)
    Status = db.Column(db.Integer)

    CustomerId = db.Column(db.Integer,db.ForeignKey('Customer.Id'))
    customer = db.relationship('CustomerModel')

    sensor = db.relationship('SensorModel',lazy='dynamic')
    
    def __init__(self,Status,CustomerId):
        self.Status = Status
        self.CustomerId = CustomerId

    def json(self):
        return {'Id': self.Id,
                'Status':self.Status,
                'CustomerId':self.CustomerId
                }

    @classmethod
    def find_by_Id(cls,Id):
        return cls.query.filter_by(Id = Id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
