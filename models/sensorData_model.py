from db import db

class SensorDataModel(db.Model):
    __tablename__ = 'SensorData'

    Id = db.Column(db.Integer, primary_key = True)
    Time = db.Column(db.String(40), nullable = False)
    Value = db.Column(db.Float,nullable = False)

    SensorId = db.Column(db.Integer,db.ForeignKey('Sensor.Id'))
    sensor = db.relationship('SensorModel')

    def __init__(self,Time,Value,SensorId):
        self.Time = Time
        self.Value = Value
        self.SensorId = SensorId

    def json(self):
        return{'Id': self.Id,
               'Time': self.Time,
               'Value': self.Value,
               'SensorId':self.SensorId
               }

    @classmethod
    def find_by_Id(cls,Id):
        return cls.query.filter_by(Id = Id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
