from flask_restful import Resource, reqparse
from flask import jsonify
from models.sensorNode_model import SensorNodeModel

class SensorNode(Resource):
    blank_message = "This field cannot be left blank"
    parser = reqparse.RequestParser()
    parser.add_argument('Status',type = int,required  = True,help = blank_message)
    parser.add_argument('CustomerId',type = int, required = True,help = blank_message)

    def get(self,Id):
        try:
            sNode = SensorNodeModel.find_by_Id(Id)
        except:
            return {'message': "An error occurred when retriving sensor node data from the database!"}, 500

        if sNode:
            return sNode.json()

        return {'message': 'Sensor Node not Found'}

    def put(self,Id):
        data = SensorNode.parser.parse_args()
        sNode = SensorNodeModel.find_by_Id(Id)
        if sNode is None:
            return {'message':'Sensor Node with id {} not found.'.format(Id)}
        else:
            sNode.Status = data['Status']
            sNode.CustomerId = data['CustomerId']

        sNode.save_to_db()
        return sNode.json()

    def delete(self,Id):
        try:
            sNode = SensorNodeModel.find_by_Id(Id)
        except:
            return {'message': 'Error when searching in the database!'},500

        if sNode:
            returnMessage = sNode
            sNode.delete_from_db()
            return {'deleted':returnMessage.json()}
        else:
            return{'message': 'Sensor Node with Id {} not found!'.format(Id)}

class AddSensorNode(Resource):
        blank_message = "This field cannot be left blank"
        parser = reqparse.RequestParser()
        parser.add_argument('Status',type = int,required  = True,help = blank_message)
        parser.add_argument('CustomerId',type = int, required = True,help = blank_message)

        def post(self):
            data = AddSensorNode.parser.parse_args()
            sNode = SensorNodeModel(**data)
            sNode.save_to_db()
            sensorNode_id = sNode.Id
            return {'message': 'Sensor Node with id {} created successfully'.format(sensorNode_id)} , 201

class SensorNodeList(Resource):
    def get(self):
        return jsonify([sNode.json() for sNode in SensorNodeModel.query.all()])
