from flask_restful import Resource, reqparse
from flask import jsonify
from models.sensor_model import SensorModel

class Sensor(Resource):
    blank_message = "This field cannot be empty!"
    parser = reqparse.RequestParser()
    parser.add_argument('SensorTypeId', type=int,required=True, help=blank_message)
    parser.add_argument('SensorNodeId', type=int,required=True, help=blank_message)

    def get(self,Id):
        try:
            sensor = SensorModel.find_by_Id(Id)
        except:
            return{'Message': 'An error occurred when retriving Sensor from the database!'},500

        if sensor:
            return sensor.json()

        return {'Message': 'Sensor with Id {} not found!'.format(Id)}

    def put(self,Id):
        data = Sensor.parser.parse_args()
        sensor = SensorModel.find_by_Id(Id)
        if sensor is None:
            return {'message':'Sensor with id {} not found.'.format(Id)}
        else:
            sensor.SensorTypeId = data['SensorTypeId']
            sensor.SensorNodeId = data['SensorNodeId']

        sensor.save_to_db()
        return sensor.json()

    def delete(self,Id):
        sensor = SensorModel.find_by_Id(Id)
        if sensor is None:
            return {'message':'Sensor with id {} not found.'.format(Id)}

        returnMessage = sensor.json()
        sensor.delete_from_db()
        return {'Deleted': returnMessage}


class AddSensor(Resource):
    blank_message = "This field cannot be empty!"
    parser = reqparse.RequestParser()
    parser.add_argument('SensorTypeId',type=int, required=True, help=blank_message)
    parser.add_argument('SensorNodeId',type=int, required=True, help=blank_message)

    def post(self):
        data = AddSensor.parser.parse_args()
        sensor = SensorModel(**data)
        sensor.save_to_db()
        sensor_id = sensor.Id
        return {'Message': 'Sensor with id {} successfully added!'.format(sensor_id)}

class SensorList(Resource):
    def get(self):
        return jsonify([sensor.json() for sensor in SensorModel.query.all()])
