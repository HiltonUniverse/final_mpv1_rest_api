from flask_restful import Resource, reqparse
from flask import jsonify
from models.sensorData_model import SensorDataModel

class SensorData(Resource):
    blank_message = "This field cannot be empty!"
    parser = reqparse.RequestParser()
    sensorData_arguments = ['Time','Value','SensorId']
    for arg in sensorData_arguments:
        parser.add_argument(arg,type = str, required = True, help = blank_message)

    def get(self,Id):
        try:
            sensorData = SensorDataModel.find_by_Id(Id)
        except:
            return{'Message': 'An error occurred when retriving Sensor Data from the database!'},500

        if sensorData:
            return sensorData.json()

        return {'message':'Sensor Data with id {} not found.'.format(Id)}


    def put(self,Id):
        data = SensorData.parser.parse_args()
        try:
            sensorData = SensorDataModel.find_by_Id(Id)
        except:
            return{'Message': 'An error occurred when retriving Sensor Data from the database!'},500

        if sensorData is None:
            return {'message':'Sensor Data with id {} not found.'.format(Id)}
        else:
            sensorData.Time = data['Time']
            sensorData.Value = data['Value']
            sensorData.SensorId = data['SensorId']

        sensorData.save_to_db()
        return sensorData.json()

    def delete(self,Id):
        sensorData = SensorDataModel.find_by_Id(Id)
        if sensorData is None:
            return {'message':'Sensor Data with id {} not found.'.format(Id)}

        returnMessage = sensorData.json()
        sensorData.delete_from_db()
        return {'Deleted': returnMessage}

class AddSensorData(Resource):
    blank_message = "This field cannot be empty!"
    parser = reqparse.RequestParser()
    sensorData_arguments = ['Time','Value','SensorId']
    for arg in sensorData_arguments:
        parser.add_argument(arg,type = str, required = True, help = blank_message)

    def post(self):
        data = AddSensorData.parser.parse_args()
        sensorData = SensorDataModel(**data)
        sensorData.save_to_db()
        return {'Message': 'SensorData with id {} successfully added!'.format(sensorData.Id)}


class SensorDataList(Resource):
    def get(self):
        return jsonify([sensorData.json() for sensorData in SensorDataModel.query.all()])
