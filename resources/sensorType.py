from flask_restful import Resource, reqparse
from flask import jsonify
from models.sensorType_model import SensorTypeModel

class SensorType(Resource):
    parser = reqparse.RequestParser()
    blank_message = "This field cannot be empty"
    sensorType_arguments = ['Type','Name','Unit','Variance']
    for sType in sensorType_arguments:
        parser.add_argument(sType,type = str,required  = True,help = blank_message)

    def get(self,Id):
        try:
            sType = SensorTypeModel.find_by_Id(Id)
        except:
           return {'message': "An error occurred when retriving SensorType data from the database!"}, 500

        if sType:
            return sType.json()

        return {'message': 'SensorType not Found'}

    def put(self,Id):
        data = SensorType.parser.parse_args()
        sType = SensorTypeModel.find_by_Id(Id)
        if sType is None:
            return {'message':'SensorType with id {} not found.'.format(Id)}
        else:
            sType.Type = data['Type']
            sType.Name = data['Name']
            sType.Unit = data['Unit']
            sType.Variance = data['Variance']

        sType.save_to_db()
        return sType.json()

    def delete(self,Id):
        sType = SensorTypeModel.find_by_Id(Id)
        if sType:
            sType.delete_from_db()

        return {'message':'SensorType with id {} Deleted'.format(Id)},

class AddSensorType(Resource):
    parser = reqparse.RequestParser()
    blank_message = "This field cannot be empty"
    sensorType_arguments = ['Type','Name','Unit','Variance']
    for sType in sensorType_arguments:
        parser.add_argument(sType,type = str,required  = True,help = blank_message)

    def post(self):
        data = AddSensorType.parser.parse_args()
        sType = SensorTypeModel.find_by_Name(data['Name'])

        if sType :
            return {'message':'sensorType with name {} already exists'.format(data['Name'])}

        newSType = SensorTypeModel(**data)
        newSType.save_to_db()
        sType_id = newSType.Id
        return {'message':'SensorType with id {} created successfully'.format(sType_id)}

class SensorTypeList(Resource):
    def get(self):
        return jsonify([sType.json() for sType in SensorTypeModel.query.all()])
