from flask_restful import Resource, reqparse
from flask import jsonify
from models.customer_model import CustomerModel

class Customer(Resource):
    blank_message = "This field cannot be left blank"
    customer_fields = ['FirstName','LastName','Email','Country','City','Street','StreetNumber']

    parser = reqparse.RequestParser()
    for customer in customer_fields:
        parser.add_argument(customer,type = str,required  = True,help = blank_message)

    def get(self,Id):
        try:
            customer = CustomerModel.find_by_Id(Id)
        except:
           return {'message': "An error occurred when retriving Customer data from the database!"}, 500

        if customer:
            return customer.json()

        return {'message': 'Customer not Found'}


    def put(self,Id):
        data = Customer.parser.parse_args()
        customer = CustomerModel.find_by_Id(Id)
        if customer is None:
            return {'message':'Customer with id {} not found.'.format(Id)}
        else:
            customer.FirstName = data['FirstName']
            customer.LastName = data['LastName']
            customer.Email = data['Email']
            customer.Country = data['Country']
            customer.City = data['City']
            customer.Street = data['Street']
            customer.StreetNumber = data['StreetNumber']

        customer.save_to_db()
        return customer.json()

    def delete(self,Id):
        customer = CustomerModel.find_by_Id(Id)
        if customer is None:
            return {'message':'Customer with id {} not Found'.format(Id)}

        returnMessage = customer.json()
        customer.delete_from_db()

        return {'Deleted': returnMessage}


class AddCustomer(Resource):
    blank_message = "This field cannot be left blank"
    customer_fields = ['FirstName','LastName','Email','Country','City','Street','StreetNumber']

    parser = reqparse.RequestParser()
    for customer in customer_fields:
        parser.add_argument(customer,type = str,required  = True,help = blank_message)

    def post(self):
        data = AddCustomer.parser.parse_args()
        if CustomerModel.find_by_Email(data['Email']):
            return {'message': "A customer with that email already exists!"}, 400

        customer = CustomerModel(**data)
        customer.save_to_db()
        customer_id = customer.Id
        return {'message': 'Customer with id {} created successfully'.format(customer_id)} , 201


class CustomerList(Resource):
    def get(self):
        return jsonify([customer.json() for customer in CustomerModel.query.all()])
